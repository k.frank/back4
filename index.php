<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         
<link rel="stylesheet" type="text/css" href="style.css" >
        <script src="localStorage.js" defer></script>

        <title>web4</title>
    </head>
    <body>
    <style>
    .error {
     border: 2px solid red;
     }
    </style>
    <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
    ?>
    <div class="main-form">
       <br>
       <br>
                <form  action="" method="post">
                   
                        <label><strong>Name</strong></label>
                        <input type="text" class="form-control" name="Name" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder=""><br>
                    
                  
                        <label><strong>Email</strong></label>
                        <input type="email" class="form-control" name="E_mail" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" placeholder=""><br>
                   
                   
                        <label><strong>Date</strong></label>
                        <select " name="Date" <?php if ($errors['data']) {print 'class="error"';} ?>>
                         <option value="2007" <?php if ($errors['sverh']and$values['sverh']=="2007") {print 'selected="selected"';}?>>2007</option>
                         <option value="2006" <?php if ($errors['sverh']and$values['sverh']=="2006") {print 'selected="selected"';}?>>2006</option>
                         <option value="2005" <?php if ($errors['sverh']and$values['sverh']=="2005") {print 'selected="selected"';}?>>2005</option>
                         <option value="2004" <?php if ($errors['sverh']and$values['sverh']=="2004") {print 'selected="selected"';}?>>2004</option>
                         <option value="2003" <?php if ($errors['sverh']and$values['sverh']=="2003") {print 'selected="selected"';}?>>2003</option>
                            <option value="2002" <?php if ($errors['sverh']and$values['sverh']=="2002") {print 'selected="selected"';}?>>2002</option>
                            <option value="2001" <?php if ($errors['sverh']and$values['sverh']=="2001") {print 'selected="selected"';}?>>2001</option>
                            <option value="2000" <?php if ($errors['sverh']and$values['sverh']=="2000") {print 'selected="selected"';}?>>2000</option>
                            <option value="1999" <?php if ($errors['sverh']and$values['sverh']=="1999") {print 'selected="selected"';}?>>1999</option>
                            <option value="1998" <?php if ($errors['sverh']and$values['sverh']=="1998") {print 'selected="selected"';}?>>1998</option>
                              <option value="1997" <?php if ($errors['sverh']and$values['sverh']=="1997") {print 'selected="selected"';}?>>1997</option>
                                <option value="1996" <?php if ($errors['sverh']and$values['sverh']=="1996") {print 'selected="selected"';}?>>1996</option>
                        </select>
                     <br>
                   		<div <?php if ($errors['pol']) {print 'class="error"';} ?>>
                        <label><strong>Gender</strong></label><br>
                     	
                            <input type="radio" name="Gender" value="Male" <?php if ($errors['pol']and$values['pol']=="Male") {print 'checked="checked"';}?>>
                            <label>
                                Male
                            </label>
                        
                        <div>
                        
                            <input type="radio" name="Gender" <?php if ($errors['pol']and$values['pol']=="Female") {print 'checked="checked"';}?> value="Female">
                            <label>
                                Female
                            </label>
                        </div>
                        </div>
                     
                     	<div  <?php if ($errors['sverh']) {print 'class="error"';} ?>>
                        <label><strong>Number of limbs</strong></label><br>
                        <input type="radio" name="Number_of_limbs" <?php if ($errors['konch']and$values['konch']=="1") {print 'checked="checked"';}?> value="1">
                        <label>1</label>
                        <input type="radio" name="Number_of_limbs" <?php if ($errors['konch']and$values['konch']=="2") {print 'checked="checked"';}?> value="2">
                        <label>2</label>
                        <input type="radio" name="Number_of_limbs" <?php if ($errors['konch']and$values['konch']=="3") {print 'checked="checked"';}?> value="3">
                        <label>3</label>
                        <input type="radio" name="Number_of_limbs" <?php if ($errors['konch']and$values['konch']=="4") {print 'checked="checked"';}?> value="4">
                        <label>4</label>
                        <input type="radio" name="Number_of_limbs" <?php if ($errors['konch']and$values['konch']=="5") {print 'checked="checked"';}?> value="8">
                        <label>8</label>
                    </div>
                    <div class="form-group">
                        <label><strong>Superpowers</strong></label><br>
                        <select multiple name="Superpowers" <?php if ($errors['sverh']) {print 'class="error"';} ?> >
                          <option value="Immortality" <?php if ($errors['sverh']and$values['sverh']=="Immortality") {print 'selected="selected"';}?>>Immortality</option>
                          <option value="Passing through walls" <?php if ($errors['sverh']and$values['sverh']=="Passing through walls") {print 'selected="selected"';}?> >Passing through walls</option>
                          <option value="Levitation" <?php if ($errors['sverh']and$values['sverh']=="Levitation") {print 'selected="selected"';}?>>Levitation</option>
                            <option value="Mind reading" <?php if ($errors['sverh']and$values['sverh']=="Mind reading") {print 'selected="selected"';}?>>Mind reading</option>
                              <option value="Teleportation" <?php if ($errors['sverh']and$values['sverh']=="Teleportation") {print 'selected="selected"';}?>>Teleportation</option>
                            
                        </select>
                    </div>
                     <br>
                    <div >
                        <label><strong>Biografia</strong></label><br>
                        <textarea name="Biografia" <?php if ($errors['bio']) {print 'class="error"';} ?>  placeholder=" text"><?php print $values['bio']; ?></textarea>
                    </div>
                    <div >
                        <input type="checkbox" name="Accept">
                        <label>
                            Accept the privacy policy
                        </label>
                    </div>
                     <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div> 
    </body>
</html>